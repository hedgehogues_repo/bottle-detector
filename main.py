# -*- coding: utf-8 -*-

from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt
import numpy as np
import cv2
import math


def Edge(image, shift):
    nX, nY = image.shape[:]
    shift = shift / 2
    answ = [[0 for x in range(0, nY)] for y in range(0, nX)]
    for x in range(shift, nX - shift):
        for y in range(shift, nY - shift):
            data = np.array(image[(x - shift) : (x + shift), (y - shift) : (y + shift)])
            v = np.var(data)
            m = np.mean(data)
            answ[x][y] = 1. / (1 + math.exp(-v ** 0.001 * (image[x, y] - m)))
    return np.array(answ)


pathBud = "data/Bud/"
pathNotBud = "data/NotBud/"
pathTemplate = "data/templates/"

templatesIm = [f for f in listdir(pathTemplate) if f != '.gitkeep' and isfile(join(pathTemplate, f))]
budIm = [f for f in listdir(pathBud) if f != '.gitkeep' and isfile(join(pathBud, f))]
notBudIm = [f for f in listdir(pathNotBud) if f != '.gitkeep' and isfile(join(pathNotBud, f))]


methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
meth = methods[1]
DimImage = 1200

count = 0
for item in budIm:
    result = []
    img = cv2.imread(pathBud + item, 0) / 255.
    hImage, wImage = img.shape[:]
    hImage, wImage = int(hImage / float(max(hImage, wImage)) * DimImage), int(
        wImage / float(max(hImage, wImage)) * DimImage)
    img = cv2.resize(img, (wImage, hImage), interpolation = cv2.INTER_CUBIC)
    img = Edge(img, 3) * 255
    method = eval(meth)
    for itemTemplate in templatesIm:
        # Считываем шаблон
        template = cv2.imread(pathTemplate + itemTemplate, 0)
        hTemplate, wTemplate = template.shape[:]
        print hTemplate, wTemplate,
        try:
            res = cv2.matchTemplate(img, template, method)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            print max_val
            result.append(max_val)
        except:
            continue
    answ = max(result)
    print item, ". Результат: ", answ
    if answ > 0.40:
        count += 1
    # plt.imshow(img, cmap = 'gray')
    # plt.show()

print count, len(budIm)