from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt
import numpy as np
import cv2
import math
import copy


def Edge(image, shift):
    nX, nY = image.shape[:]
    shift = shift
    answ = [[0 for x in range(0, nY)] for y in range(0, nX)]
    for x in range(shift, nX - shift):
        for y in range(shift, nY - shift):
            data = np.array(image[(x - shift) : (x + shift), (y - shift) : (y + shift)])
            v = np.var(data)
            m = np.mean(data)
            answ[x][y] = 1. / (1 + math.exp(-v * (image[x, y] + m)))
    return np.array(answ)

pathTemplateRead = "data/templates/etalon/"
pathTemplateWrite = "data/templates/"

templatesIm = [f for f in listdir(pathTemplateRead) if f != '.gitkeep' and isfile(join(pathTemplateRead, f))]
template = cv2.imread(pathTemplateRead + templatesIm[0], 0) / 255.0
template = Edge(template, 3) * 255
template = cv2.GaussianBlur(template, (3, 3), 0)
hTemplate, wTemplate = template.shape[:]
plt.imshow(template, cmap='gray')
plt.show()

maxDemensional = 510
minDemensional = 130

listRatio = range(minDemensional, maxDemensional, 20)
templates = []

for ratio in listRatio:
    hTemplate, wTemplate = int(hTemplate / float(max(hTemplate, wTemplate)) * ratio), int(wTemplate / float(max(hTemplate, wTemplate)) * ratio)
    templates.append(cv2.resize(template, (wTemplate, hTemplate), interpolation = cv2.INTER_CUBIC))
    print templates[-1].shape[:]
    cv2.imwrite(pathTemplateWrite + str(ratio) + '.png', templates[-1])